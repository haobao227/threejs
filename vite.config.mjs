import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import glsl from 'vite-plugin-glsl';
function pathResolve(dir) {
  return resolve(process.cwd(), '.', dir)
}
// https://vitejs.dev/config/
export default defineConfig(() => {
  return {
    plugins: [glsl(), vue()],
    // 路径重定向
    resolve: {
      alias: [
        {
          find: "@",
          replacement: pathResolve('src')
        },
      ],
      dedupe: ["vue"],
    },
  }
})
