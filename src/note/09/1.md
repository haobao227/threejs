

# new THREE.Raycaster(origin, direction)方法 

射线由 起点（origin） 和 方向（direction） 定义。


```
const raycaster = new THREE.Raycaster();

 - 无参数时，默认创建一个空的 Raycaster。
 - 可以通过 set 方法设置射线的起点和方向。

# 可以通过后set设置射线的起点和方向。
const origin = new THREE.Vector3(0, 0, 0);
const direction = new THREE.Vector3(0, 0, -1).normalize();
raycaster.set(origin, direction);

```
# 从相机发射射线，方向指向屏幕上的某个点（通常用于鼠标拾取）。
# 2. setFromCamera(coords, camera)  

参数：
`coords`：屏幕坐标（THREE.Vector2 类型），范围是 [-1, 1]。
`camera`：相机对象（THREE.Camera 类型）。

```
const mouse = new THREE.Vector2(); // 鼠标的屏幕坐标
raycaster.setFromCamera(mouse, camera);
```

# 3. intersectObject(object, recursive) 
# 检测射线与单个对象的交点
参数：
- `object`：要检测的对象（THREE.Object3D 类型）。
- `recursive`：是否递归检测子对象（布尔类型，默认为 false）。
返回值：
- 返回一个数组，包含所有与射线相交的对象及其交点信息。

```
const objects = [mesh1, mesh2, mesh3];
const targetList = raycaster.intersectObjects(objects);
console.log(targetList);

```

# 4. 检测结果的格式

`targetList` 和 `intersectObjects` 返回的数组中的每个元素是一个对象，包含以下属性：
- `distance`：射线起点到交点的距离。
- `point`：交点的世界坐标（THREE.Vector3 类型）。
- `face`：相交的面（THREE.Face 类型）。
- `faceIndex`：相交的面的索引。
- `object`：相交的对象（THREE.Object3D 类型）。
- `uv`：相交点的纹理坐标（THREE.Vector2 类型）。