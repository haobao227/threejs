import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import Stats from 'three/examples/jsm/libs/stats.module'
import { HeartCurve } from 'three/examples/jsm/curves/CurveExtras';
import * as dat from 'dat.gui'
const $ = {
    createScene() {
        const canvas = document.getElementById('canvas');
        const width = window.innerWidth;
        const height = window.innerHeight;
        canvas.width = width;
        canvas.height = height;
        // 创建3D场景
        const scene = new THREE.Scene();
        this.width = width
        this.height = height
        this.canvas = canvas
        this.scene = scene
    },
    createLights() {
        const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
        const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
        this.scene.add(ambientLight, directionalLight);
    },
    curveGenerator() {
     // 生成一个心形曲线   
     const curve = new HeartCurve(1);
     // 缓冲几何体 // 3d路径  管道分段数(越多越精细)  管道的半径(粗细) 是否闭合 
     const tubeGeometry = new THREE.TubeGeometry(curve, 200 , 0.01, 8 , true)

     const material = new THREE.MeshBasicMaterial({
         color: 0x00ff00
     })

     const tubeMesh = new THREE.Mesh(tubeGeometry, material)
     // 将曲线分割成1000段
     const points = curve.getPoints(1000);
     const index = 0;
     const point = points[index];

     this.cameraIndex.position.set();
     this.scene.add(tubeMesh);
     this.curve = curve;
    },
    createObjects() {
        const geometry = new THREE.BoxGeometry(1, 1, 1);
        const material = new THREE.MeshLambertMaterial({
            color: 0x1890ff
        });
        //创建3D物体对象
        const mesh = new THREE.Mesh(geometry, material);
        // 计算一下盒向量
        mesh.geometry.computeBoundingBox()
        this.scene.add(mesh);
        this.mesh = mesh;
    },
    createCamera() {
        // const size = 4
        // 创建透视相机
        const pCamera = new THREE.PerspectiveCamera(75, this.width / this.height, 1, 10);
        pCamera.position.set(0, 0, 3)
        pCamera.lookAt(this.scene.position)
        this.pCamera = pCamera
        this.camera = pCamera
        this.scene.add(pCamera)

        const watcherCamera = new THREE.PerspectiveCamera(75, this.width / this.height, 0.1, 100); 
        watcherCamera.position.set(2,2,20)
        watcherCamera.lookAt(this.scene.position);
        this.watcherCamera = watcherCamera;
        this.scene.add(watcherCamera);
        this.camera = watcherCamera;
        
    },
    controls() {
        // 创建轨道控制器
        const orbitControls = new OrbitControls(this.camera, this.canvas);
        // 惯性控制
        orbitControls.enableDamping = true;
        this.orbitControls = orbitControls;
    },
    helpers() {
        // 创建辅助坐标系
        const axe = new THREE.AxesHelper();
        // 相机辅助线
        // const cameraHelper = new THREE.CameraHelper(this.watcherCamera)
        this.scene.add(axe);
        // this.cameraHelper = cameraHelper;
    },
    datGui() {
        const _this = this
        const gui = new dat.GUI();
        const params = {
            color: 0x1890ff,
            wireframe: false,
            switchCamera() { // 切换相机回调函数
                if(_this.camera.type === 'OrthographicCamera') {
                    _this.camera = _this.watcherCamera
                    _this.orbitControls.enabled = true;
                } else {
                    _this.camera = _this.orthoCamera;
                    _this.orbitControls.enabled = false;
                }

            },
        }
        // 相机视角 角度越大看见的越广东西越小
        gui.add(this.camera, 'fov', 40, 150, 1).onChange(val => {
            this.camera.fov = val;
            this.camera.updateProjectionMatrix()
        });
        gui.add(this.camera.position, 'x', 0.1, 10, 1).name('positionX')// 别名
        gui.add(this.camera, 'near', 0.01, 10, 0.01).onChange((val => {
            console.log(val);
            this.camera.near = val;
            this.camera.updateProjectionMatrix()
        }))
        gui.add(this.camera, 'far', 10, 100, 1).onChange((val => {
            console.log(val);
            this.camera.far = val;
            this.camera.updateProjectionMatrix()
        }))
        gui.add(this.camera, 'zoom', 0.1, 10, 0.1).onChange((val => {
            console.log(val);
            this.camera.zoom = val;
            this.camera.updateProjectionMatrix()
        }))
        gui.add(params, 'wireframe').onChange((val => {
            // 立方体的框线
            this.mesh.material.wireframe = val;
        }))
        gui.add(params, 'switchCamera')
        gui.addColor(params, 'color').onChange(val => {
            // console.log(val, _this.mesh);
            _this.mesh.material.color.set(val);

        })
    },
    render() {
        //创建渲染器
        const renderer = new THREE.WebGLRenderer({
            canvas: this.canvas,
            antialias: true // 抗锯齿
        });
        // 设置渲染器屏幕像素比
        renderer.setPixelRatio(window.devicePixelRatio || 1)
        // 设置渲染器大小
        renderer.setSize(this.width, this.height);
        // 执行渲染
        renderer.render(this.scene, this.camera);
        this.renderer = renderer
    },
    tick() {
        this.mesh.rotation.y += 0.1
        this.orbitControls.update()
        // this.cameraHelper.update()
        this.renderer.render(this.scene, this.camera);
        window.requestAnimationFrame(() => this.tick());
    },
    fitView() {
        window.addEventListener('resize', () => {
            this.camera.aspect = window.innerWidth / window.innerHeight;
            this.camera.updateProjectionMatrix();
            this.renderer.render(window.innerWidth, window.innerHeight);
        }, false)
    },
    init() {
        this.createScene()
        this.createLights()
        this.createObjects()
        this.createCamera()
        this.curveGenerator()
        this.helpers()
        this.datGui()
        this.render()
        this.controls()
        this.tick()
        this.fitView()
    }
}

export default $
