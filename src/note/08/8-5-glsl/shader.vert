/*
attribute float size;
varying float vSize;
varying vec3 vColor;   // 定义颜色变量
uniform float pointSize;
varying float cameraDistance;


void main() {
  vSize = size;
  vColor = color; // 变量赋值为attributes中的color
  // gl_Position, gl_PointSize, gl_FragColor: webgl着色器内置变量
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0); // "gl_Position"变量指定了点在屏幕上的位置，由点的位置、模型视图矩阵和投影矩阵相乘得到
  vec4 worldPosition = modelMatrix * vec4(position, 1.0); // 模型矩阵与点的位置相乘，可以得到这个点在世界坐标系的位置
  vec3 viewVector = cameraPosition - worldPosition.xyz; // 计算相机位置与当前点位置之间的向量, 将相机位置向量减去点位置向量，得到个新的向量，这个向量代表了从当前点位置到相机位置的向量
  cameraDistance = length(viewVector); // 计算当前点到相机的距离。"length()"是个内置函数，它用来计算向量的长度。它的实现方式是将向量的每个分量的平方相加，然后取平方根，即：length(vec3(x, y, z)) = sqrt(x * x + y * y + z * z). 这个距离可以用来控制点的大小，以实现透视效果，即距离相机越远的点看起来越小，距离相机越近的点看起来越大。

  gl_PointSize = vSize / cameraDistance;
}
 */

varying vec3 vColor;   // 定义颜色变量
uniform float pointSize;
varying float cameraDistance;

void main() {
  vColor = color; // 变量赋值为attributes中的color
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);

  vec4 worldPosition = modelMatrix * vec4(position, 1.0);
  vec3 viewVector = cameraPosition - worldPosition.xyz;
  // 计算相机距离
  cameraDistance = length(viewVector); // three.js 着色器材质glsl内置函数
  gl_PointSize = pointSize / cameraDistance;
}
