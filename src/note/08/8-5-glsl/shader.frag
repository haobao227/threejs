
precision highp float;

uniform vec3 color;
uniform sampler2D pointTexture;
uniform vec3 fogColor; // 雾的颜色
uniform float fogNear; // 雾的近处距离
uniform float fogFar; // 雾的远处距离

varying vec3 vColor;

/*
  #ifdef USE_FOG // 若scene上已有fog属性
    // gl_FragCoord.w 是裁剪空间 clip.w 的倒数即 1/clip.w , 由上面的透视投影矩阵的推导过程可以看出，为了凑透视除法， clip.w 值就是 眼坐标系 z 值的负数，也就是距离相机的距离。

    // 1.由上,我们可以知道gl_FragCoord.z / gl_FragCoord.w就表示当前片元和camera之间的距离即深度;

    // 2.再由smoothstep( fogNear, fogFar, depth )来得到平滑的因子fogFactor, 深度越大，该因子值越大。

    // 3.再通过mix来混合得到最终的rgb值

    #ifdef USE_LOGDEPTHBUF_EXT
      float depth = gl_FragDepthEXT / gl_FragCoord.w;
    #else
      float depth = gl_FragCoord.z / gl_FragCoord.w;
    #endif
    float fogFactor = smoothstep( fogNear, fogFar, depth );
    gl_FragColor.rgb = mix( gl_FragColor.rgb, fogColor, fogFactor ); // 返回线性混合的值 即fogFactor值越大，fogColor占比越大。假设fogColor为黑色，那么综合上面所讲，这三句代码的意思就是越远越暗，符合我们对雾效的定义和预期。
  #endif
 */

void main() {
  vec4 color = vec4( color * vColor, 1.0 ) * texture2D( pointTexture, gl_PointCoord );

	gl_FragColor = color;

  float depth = gl_FragCoord.z / gl_FragCoord.w; // 计算当前片元的深度值(从相机到片元的距离). gl_FragCoord是个内置变量，它包含了当前片元的屏幕坐标(x, y, z, w)，其中z表示深度值，w表示透视除法的分母。将z值除以w值，得到当前片元的深度值。
  float fogFactor = smoothstep( fogNear, fogFar, depth ); // smoothstep()函数会根据这三个参数计算出1介于0和1之间的值，这个值越接近1，表示当前片元越接近雾的结束位置(这里就是我们的相机)，雾因子就越大，雾的强度就越强。
  gl_FragColor.rgb = mix( gl_FragColor.rgb, fogColor, fogFactor ); // GLSL内置函数mix(), 将当前片元的颜色和雾颜色进行混合，新的颜色值, 从而实现雾化效果。 gl_FragColor.rgb表示当前片元的颜色, 当雾因子为0时，表示当前片元不受雾的影响，此时mix()函数返回的颜色值就是当前片元的颜色(也就是白色)；当雾因子为1时，表示当前片元完全被雾所覆盖，此时mix()函数返回的颜色值就是雾的颜色(也就是黑色)；当雾因子在0和1之间时，表示当前片元受到部分雾的影响，此时mix()函数返回的颜色值就是当前片元颜色和雾颜色的线性混合结果(也就是灰色)。
}

// 雾化效果资料: https://webglfundamentals.org/webgl/lessons/zh_cn/webgl-fog.html
